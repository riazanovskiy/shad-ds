package lib

import (
	"fmt"

	"gitlab.com/slon/shad-ds/projects/interfaces"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
)

type ActorContext struct {
	actorSystem *ActorSystem
	id          string
}

func (ac *ActorContext) Send(payload proto.Message, receiver string) (err error) {
	actorSystem := ac.actorSystem
	if !actorSystem.actorExists(receiver) {
		return fmt.Errorf("actor %q does not exist", receiver)
	}

	serializedMessage, err := proto.Marshal(payload)
	if err != nil {
		return err
	}

	if len(serializedMessage) > interfaces.MaxMessageSize {
		return fmt.Errorf("too large message size %v bytes", len(serializedMessage))
	}

	thunk := proto.Clone(payload)
	proto.Reset(thunk)

	sentMessage := SentMessage{
		Payload:      serializedMessage,
		PayloadThunk: thunk,
		Sender:       ac.id,
		Receiver:     receiver,
	}

	ac.actorSystem.sentMessages <- sentMessage

	return err
}

func (ac *ActorContext) Log() *zap.SugaredLogger {
	return ac.actorSystem.log
}
