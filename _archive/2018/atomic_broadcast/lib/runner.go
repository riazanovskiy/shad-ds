package lib

import (
	"container/heap"
	"fmt"
	"io"
	"math"
	"math/rand"
	"runtime/debug"
	"testing"
	"time"
)

//////////////////////////////////////////////////////////////////////////////////

type TestContext struct {
	NodeId int
	Alive  bool
	State  *TestState

	PersistentStorage []byte

	// Messages delivered on all incarnations of this node.
	Delivered   [][]int
	Incarnation int

	CurrentWrapper *TestContextWrapper
	CurrentNode    AtomicBroadcaster

	StableMessages                map[int]bool
	UnconfirmedStableMessageCount int
}

func NewTestContext(nodeId int, testState *TestState) *TestContext {
	context := &TestContext{
		PersistentStorage: make([]byte, 0),
		NodeId:            nodeId,
		Alive:             true,
		State:             testState,
		Delivered:         make([][]int, 1),
		Incarnation:       -1}

	context.Revive()
	return context
}

func (context *TestContext) ProcessTestEvent(testEvent *TestEvent) {
	switch testEvent.Type {
	case Broadcast:
		if context.Alive {
			context.CurrentNode.Broadcast(testEvent.MessageId)
		}

	case Death:
		if context.Alive {
			context.Alive = false
			context.CurrentWrapper.Alive = false
			context.CurrentWrapper = nil
			context.CurrentNode = nil
		}

	case Revival:
		if !context.Alive {
			context.Alive = true
			context.Revive()
		}

	default:
		panic("Unknown test event")
	}
}

func (context *TestContext) Revive() {
	context.Alive = true
	context.Incarnation++
	context.Delivered = append(context.Delivered, make([]int, 0))
	wrapper := &TestContextWrapper{Alive: true, Context: context}
	deliver := func(messageId int) {
		wrapper.Deliver(messageId)
	}
	context.CurrentWrapper = wrapper
	context.CurrentNode = context.State.Ctor(
		context.NodeId,
		context.State.NodeCount,
		deliver,
		wrapper)
}

func (context *TestContext) OnMessage(msg *SentMessage) {
	if context.Alive {
		if msg.TimerEvent {
			cb := msg.Message.(TimerCallback)
			cb()
		} else {
			context.CurrentNode.OnMessage(msg.Message, msg.Source)
		}
	}
}

func (context *TestContext) RegisterStableBroadcasts(broadcasts []BroadcastRequest) {
	context.UnconfirmedStableMessageCount = len(broadcasts)
	context.StableMessages = make(map[int]bool)
	for _, broadcast := range broadcasts {
		context.StableMessages[broadcast.MessageId] = false
	}
}

func (context *TestContext) DeliveredAllStableBroadcasts() bool {
	return context.UnconfirmedStableMessageCount == 0
}

func (context *TestContext) SendMessage(message interface{}, destination int) {
	if destination < 0 || destination > context.State.NodeCount {
		err := fmt.Errorf("Cannot send message to node with id %v\n", destination)
		panic(err)
	}
	sentMessage := SentMessage{
		Message:          message,
		Source:           context.NodeId,
		Destination:      destination,
		SendTimestamp:    context.State.Timestamp,
		DeliverTimestamp: context.State.Timestamp + AvgDeliveryTime,
		TimerEvent:       false}

	context.State.ApplyDelayFunc(&sentMessage)
	context.State.Queue.EnqueueMessage(sentMessage)
}

func (context *TestContext) AfterFunc(duration int, f TimerCallback) {
	sentMessage := SentMessage{
		Message:          f,
		Source:           context.NodeId,
		Destination:      context.NodeId,
		SendTimestamp:    context.State.Timestamp,
		DeliverTimestamp: context.State.Timestamp + duration,
		TimerEvent:       true}

	context.State.ApplyDelayFunc(&sentMessage)
	context.State.Queue.EnqueueMessage(sentMessage)
}

func (context *TestContext) WriteAt(p []byte, off int64) (n int, err error) {
	offset := int(off)
	if len(context.PersistentStorage) < offset+len(p) {
		size := offset + len(p) - len(context.PersistentStorage)
		context.PersistentStorage = append(context.PersistentStorage, make([]byte, size)...)
	}

	n = copy(context.PersistentStorage[offset:offset+len(p)], p)
	if n != len(p) {
		err = fmt.Errorf("Fatal error during writing to persistent storage")
	}
	return
}

func (context *TestContext) ReadAt(p []byte, off int64) (n int, err error) {
	offset := int(off)
	if len(context.PersistentStorage) < offset {
		n = 0
		err = fmt.Errorf("Offset is too large")
		return
	}

	n = len(p)
	if len(context.PersistentStorage)-offset < n {
		n = len(context.PersistentStorage) - offset
		err = io.EOF
	}

	copy(p, context.PersistentStorage[offset:offset+n])
	return
}

func (context *TestContext) Deliver(messageId int) {
	context.Delivered[context.Incarnation] = append(context.Delivered[context.Incarnation], messageId)
	value, ok := context.StableMessages[messageId]
	if ok && !value {
		context.UnconfirmedStableMessageCount--
		context.StableMessages[messageId] = true
	}
}

//////////////////////////////////////////////////////////////////////////////////

type TestContextWrapper struct {
	Alive   bool
	Context *TestContext
}

func (wrapper *TestContextWrapper) SendMessage(message interface{}, destination int) {
	if !wrapper.Alive {
		return
	}

	wrapper.Context.SendMessage(message, destination)
}

func (wrapper *TestContextWrapper) AfterFunc(duration int, f TimerCallback) {
	if !wrapper.Alive {
		return
	}

	wrapper.Context.AfterFunc(duration, f)
}

func (wrapper *TestContextWrapper) RandomInt(n int) int {
	return rand.Intn(n)
}

func (wrapper *TestContextWrapper) WriteAt(p []byte, off int64) (n int, err error) {
	if !wrapper.Alive {
		n = 0
		err = fmt.Errorf("Peer is dead")
		return
	}

	return wrapper.Context.WriteAt(p, off)
}

func (wrapper *TestContextWrapper) ReadAt(p []byte, off int64) (n int, err error) {
	if !wrapper.Alive {
		n = 0
		err = fmt.Errorf("Peer is dead")
		return
	}

	return wrapper.Context.ReadAt(p, off)
}

func (wrapper *TestContextWrapper) Deliver(messageId int) {
	if !wrapper.Alive {
		return
	}

	wrapper.Context.Deliver(messageId)
}

func (wrapper *TestContextWrapper) GetStorageLength() int {
	if !wrapper.Alive {
		return 0
	}

	return len(wrapper.Context.PersistentStorage)
}

//////////////////////////////////////////////////////////////////////////////////

type EventType int

const (
	Death        EventType = 0
	Revival      EventType = 1
	Broadcast    EventType = 2
	FinishWarmup EventType = 3
)

type TestEvent struct {
	Node      int
	Timestamp int
	Type      EventType
	MessageId int
}

type QueueImpl []interface{}

func (q QueueImpl) Len() int { return len(q) }

func (q QueueImpl) Less(i, j int) bool {
	getTimestamp := func(item interface{}) int {
		switch x := item.(type) {
		case SentMessage:
			return x.DeliverTimestamp

		case TestEvent:
			return x.Timestamp
		}

		panic(fmt.Errorf("Unreachable"))
	}

	return getTimestamp(q[i]) < getTimestamp(q[j])
}

func (q QueueImpl) Swap(i, j int) {
	q[i], q[j] = q[j], q[i]
}

func (q *QueueImpl) Push(x interface{}) {
	*q = append(*q, x)
}

func (q *QueueImpl) Pop() interface{} {
	old := *q
	n := len(old)
	item := old[n-1]
	*q = old[0 : n-1]
	return item
}

//////////////////////////////////////////////////////////////////////////////////

type CombinedQueue struct {
	queueImpl  QueueImpl
	eventCount int
}

func NewCombinedQueue() *CombinedQueue {
	queue := &CombinedQueue{
		queueImpl:  make(QueueImpl, 0, 10),
		eventCount: 0}

	heap.Init(&queue.queueImpl)
	return queue
}

func (queue *CombinedQueue) EnqueueBroadcasts(broadcasts []BroadcastRequest, currentTimestamp int) {
	for _, broadcast := range broadcasts {
		event := TestEvent{
			Node:      broadcast.Source,
			Timestamp: broadcast.Timestamp + currentTimestamp,
			Type:      Broadcast,
			MessageId: broadcast.MessageId}
		heap.Push(&queue.queueImpl, event)
		queue.eventCount++
	}
}

func (queue *CombinedQueue) EnqueueNodeDeaths(nodeEvents []NodeEvent, currentTimestamp int) {
	for _, death := range nodeEvents {
		event := TestEvent{
			Node:      death.Node,
			Timestamp: death.Timestamp + currentTimestamp,
			Type:      Death}
		heap.Push(&queue.queueImpl, event)
		queue.eventCount++
	}
}

func (queue *CombinedQueue) EnqueueFinishWarmup(timestamp int) {
	event := TestEvent{
		Node:      -1,
		Timestamp: timestamp,
		Type:      FinishWarmup}
	heap.Push(&queue.queueImpl, event)
	queue.eventCount++
}

func (queue *CombinedQueue) EnqueueNodeRevivals(nodeEvents []NodeEvent, currentTimestamp int) {
	for _, revival := range nodeEvents {
		event := TestEvent{
			Node:      revival.Node,
			Timestamp: revival.Timestamp + currentTimestamp,
			Type:      Revival}
		heap.Push(&queue.queueImpl, event)
		queue.eventCount++
	}
}

func (queue *CombinedQueue) EnqueueMessage(sentMessage SentMessage) {
	heap.Push(&queue.queueImpl, sentMessage)
}

func (queue *CombinedQueue) GetNext() interface{} {
	if len(queue.queueImpl) == 0 {
		return nil
	}

	item := heap.Pop(&queue.queueImpl)
	if _, ok := item.(TestEvent); ok {
		queue.eventCount--
	}
	return item
}

func (queue CombinedQueue) HasEnqueuedEvents() bool {
	return queue.eventCount > 0
}

//////////////////////////////////////////////////////////////////////////////////

var DelayRand *rand.Rand = rand.New(rand.NewSource(7919))

func DefaultDelayFunc(sentMessage SentMessage) int {
	delay := sentMessage.DeliverTimestamp - sentMessage.SendTimestamp
	r := DelayRand.NormFloat64()
	r = r*2 - 1.0 // get random in range [-1, 1]
	r = r * 0.05  // get random in range [-0.05, 0.05]
	delta := int(float64(delay) * r)
	return sentMessage.DeliverTimestamp + delta
}

//////////////////////////////////////////////////////////////////////////////////

type PhaseType int

const (
	Warmup   PhaseType = 0
	Stable   PhaseType = 1
	Unstable PhaseType = 2
	Teardown PhaseType = 3
)

type TestState struct {
	Timestamp           int
	CurrentSegment      *TestSegment
	CurrentSegmentIndex int
	Phase               PhaseType
	Ctor                AtomicBroadcasterCtor
	NodeCount           int
	Queue               *CombinedQueue
	TestContexts        []*TestContext
}

func (state TestState) ApplyDelayFunc(sentMessage *SentMessage) {

	validateNoJumpsIntoPast := func(ts int) {
		if ts <= sentMessage.SendTimestamp {
			panic(fmt.Errorf("Invalid delay func in unstable sub-segment %v; deliver timestamp (%v) must be greater than send timestamp (%v)",
				state.CurrentSegmentIndex,
				ts,
				sentMessage.DeliverTimestamp))
		}
	}

	validateBoundedDrift := func(ts int) {
		originalDelay := sentMessage.DeliverTimestamp - sentMessage.SendTimestamp
		actualDelay := ts - sentMessage.SendTimestamp

		delta := originalDelay - actualDelay
		drift := float64(delta) / float64(originalDelay)
		if math.Abs(drift) > StableSegmentError {
			panic(fmt.Errorf("Invalid delay func in stable sub-segment %v; deliver timestamp drift (%v) is too large ",
				state.CurrentSegmentIndex,
				drift))
		}
	}

	switch {
	case state.CurrentSegment == nil:
		timestamp := DefaultDelayFunc(*sentMessage)
		sentMessage.DeliverTimestamp = timestamp
	case state.Phase == Unstable:
		timestamp := state.CurrentSegment.UnstableDelayFunc(*sentMessage)
		validateNoJumpsIntoPast(timestamp)
		sentMessage.DeliverTimestamp = timestamp
	default:
		timestamp := state.CurrentSegment.StableDelayFunc(*sentMessage)
		validateNoJumpsIntoPast(timestamp)
		validateBoundedDrift(timestamp)
		sentMessage.DeliverTimestamp = timestamp
	}
}

func (state TestState) ValidateActiveQuorum() error {
	aliveNodeCount := 0
	for _, context := range state.TestContexts {
		if context.Alive {
			aliveNodeCount++
		}
	}

	if aliveNodeCount <= state.NodeCount/2 {
		return fmt.Errorf("Invalid test: not enough alive nodes in the beginning of segment %v", state.CurrentSegmentIndex)
	}

	return nil
}

func (state TestState) ValidateStableBroadcasts(broadcasts []BroadcastRequest, segmentIndex int) error {
	for _, broadcast := range broadcasts {
		context := state.TestContexts[broadcast.Source]
		if !context.Alive {
			return fmt.Errorf("Invalid test: cannot broadcast message with id %v on dead node %v in stable segment %v",
				broadcast.MessageId,
				broadcast.Source,
				segmentIndex)
		}
	}

	return nil
}

func (state *TestState) ReviveAll() {
	for _, context := range state.TestContexts {
		if !context.Alive {
			context.Revive()
		}
	}
}

func (state *TestState) RunEventLoopWhile(condition func() bool) {
	for condition() {
		event := state.Queue.GetNext()
		switch x := event.(type) {
		case SentMessage:
			state.Timestamp = x.DeliverTimestamp
			context := state.TestContexts[x.Destination]
			if x.TimerEvent {
				// Call delayed callback.
				cb := x.Message.(TimerCallback)
				cb()
			} else {
				context.OnMessage(&x)
			}

		case TestEvent:
			state.Timestamp = x.Timestamp
			if x.Type != FinishWarmup {
				context := state.TestContexts[x.Node]
				context.ProcessTestEvent(&x)
			}
		}
	}
}

func (state *TestState) ValidateAllDeliveries() error {
	context := state.TestContexts[0]
	for i := 1; i < state.NodeCount; i++ {
		other := state.TestContexts[i]
		if len(context.Delivered[context.Incarnation]) != len(other.Delivered[other.Incarnation]) {
			return fmt.Errorf("Different number of delivered messages in nodes %v and %v",
				context.NodeId,
				other.NodeId)
		}

		for logIndex := 0; logIndex < len(context.Delivered[context.Incarnation]); logIndex++ {
			if context.Delivered[context.Incarnation][logIndex] != other.Delivered[other.Incarnation][logIndex] {
				return fmt.Errorf("Nodes %v and %v have different message delivered at log position %v",
					context.NodeId,
					other.NodeId,
					logIndex)
			}
		}
	}

	for _, context := range state.TestContexts {
		for incarnation := 0; incarnation < context.Incarnation; incarnation++ {
			for logIndex := 0; logIndex < len(context.Delivered[incarnation]); logIndex++ {
				if context.Delivered[context.Incarnation][logIndex] != context.Delivered[incarnation][logIndex] {
					return fmt.Errorf("Node %v has different message delivered at log position %v in incarnations %v and %v (%v != %v)",
						context.NodeId,
						logIndex,
						context.Incarnation,
						incarnation,
						context.Delivered[context.Incarnation][logIndex],
						context.Delivered[incarnation][logIndex])
				}
			}
		}
	}

	return nil
}

//////////////////////////////////////////////////////////////////////////////////

func ValidateUniqueMessageIds(segments []TestSegment) error {
	uniqueIds := map[int]bool{}

	totalBroadcastCount := 0
	for _, segment := range segments {
		for _, broadcast := range segment.StableBroadcasts {
			_, ok := uniqueIds[broadcast.MessageId]
			totalBroadcastCount++
			if ok {
				return fmt.Errorf("Invalid test: Duplicate message ids in broadcast requests (MessageId: %v)", broadcast.MessageId)
			}
			uniqueIds[broadcast.MessageId] = true
		}

		for _, broadcast := range segment.UnstableBroadcasts {
			_, ok := uniqueIds[broadcast.MessageId]
			totalBroadcastCount++
			if ok {
				return fmt.Errorf("Invalid test: Duplicate message ids in broadcast requests (MessageId: %v)", broadcast.MessageId)
			}
			uniqueIds[broadcast.MessageId] = true
		}
	}

	if totalBroadcastCount > MaxBroadcastsPerTest {
		return fmt.Errorf("Invalid test: total number of broadcasts is too large (BroadcastCount: %v)", totalBroadcastCount)
	}

	return nil
}

func ValidateEventCount(segments []TestSegment) error {
	totalEventCount := 0
	for _, segment := range segments {
		totalEventCount += len(segment.DeathEvents)
		totalEventCount += len(segment.ReviveEvents)
	}

	if totalEventCount > MaxEventsPerTest {
		return fmt.Errorf("Invalid test: total number of deaths and revivals is too large (EventCount: %v)", totalEventCount)
	}

	return nil
}

func RunTest(test TestDescriptor, nodeCtor AtomicBroadcasterCtor) (err error) {
	const RandomSeed = 7919
	DelayRand.Seed(RandomSeed)
	rand.Seed(RandomSeed)

	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("%v\nStack:\n%s\n", e, debug.Stack())
		}
	}()

	if test.NodeCount < 1 {
		return fmt.Errorf("Invalid test: NodeCount must be greater than zero (NodeCount: %v)",
			test.NodeCount)
	}

	if test.NodeCount > MaxNodeCount {
		return fmt.Errorf("Invalid test: NodeCount must not be greater than MaxNodeCount (NodeCount: %v)",
			test.NodeCount)
	}

	if len(test.Segments) > MaxSegmentsPerTest {
		return fmt.Errorf("Invalid test: number of segments in test should not exceed than MaxSegmentsPerTest")
	}

	if err = ValidateUniqueMessageIds(test.Segments); err != nil {
		return
	}

	if err = ValidateEventCount(test.Segments); err != nil {
		return
	}

	if len(test.Segments) < 1 {
		return
	}

	testState := &TestState{
		Timestamp:    0,
		Ctor:         nodeCtor,
		Phase:        Warmup,
		NodeCount:    test.NodeCount,
		Queue:        &CombinedQueue{},
		TestContexts: make([]*TestContext, test.NodeCount)}

	for i := 0; i < testState.NodeCount; i++ {
		testState.TestContexts[i] = NewTestContext(i, testState)
	}

	for segmentIndex, segment := range test.Segments {
		fmt.Printf("==== Start segment %v at timestamp %v\n", segmentIndex, testState.Timestamp)

		if err = testState.ValidateActiveQuorum(); err != nil {
			return
		}

		testState.CurrentSegment = &segment
		testState.CurrentSegmentIndex = segmentIndex
		testState.Phase = Warmup

		warmupTimestamp := testState.Timestamp + AvgDeliveryTime*WarmupRounds
		testState.Queue.EnqueueFinishWarmup(warmupTimestamp + 1)
		testState.RunEventLoopWhile(func() bool {
			return testState.Timestamp < warmupTimestamp
		})

		fmt.Printf("==== Finished warmup segment %v at timestamp %v\n", segmentIndex, testState.Timestamp)

		testState.Phase = Stable
		if err = testState.ValidateStableBroadcasts(segment.StableBroadcasts, segmentIndex); err != nil {
			return
		}
		testState.Queue.EnqueueBroadcasts(segment.StableBroadcasts, testState.Timestamp)
		for _, context := range testState.TestContexts {
			context.RegisterStableBroadcasts(segment.StableBroadcasts)
		}

		testState.RunEventLoopWhile(func() bool {
			if testState.Queue.HasEnqueuedEvents() {
				return true
			}
			for _, context := range testState.TestContexts {
				if context.Alive && !context.DeliveredAllStableBroadcasts() {
					return true
				}
			}
			return false
		})

		fmt.Printf("==== Finished stable sub-segment segment %v  at timestamp %v\n", segmentIndex, testState.Timestamp)

		testState.Phase = Unstable
		testState.Queue.EnqueueBroadcasts(segment.UnstableBroadcasts, testState.Timestamp)
		testState.Queue.EnqueueNodeDeaths(segment.DeathEvents, testState.Timestamp)
		testState.Queue.EnqueueNodeRevivals(segment.ReviveEvents, testState.Timestamp)

		testState.RunEventLoopWhile(func() bool {
			if testState.Queue.HasEnqueuedEvents() {
				return true
			}
			return false
		})
	}

	testState.Phase = Teardown
	testState.CurrentSegmentIndex++
	testState.CurrentSegment = nil
	teardownTimestamp := testState.Timestamp + AvgDeliveryTime*TeardownRounds
	testState.ReviveAll()
	testState.RunEventLoopWhile(func() bool {
		return testState.Timestamp < teardownTimestamp
	})

	err = testState.ValidateAllDeliveries()
	return
}

func RunTests(t *testing.T, algo AtomicBroadcasterCtor, tests []TestDescriptor, shouldFail bool) {
	var failed bool

	startTime := time.Now()

	for _, test := range tests {
		if time.Since(startTime) > TestSuitTimeout {
			t.Errorf("Test suit timed out")
		}

		fmt.Printf("==== Running test %v by %v\n", test.TestName, test.Author)

		t.Run(fmt.Sprintf("%q by %s", test.TestName, test.Author), func(t *testing.T) {
			resultChannel := make(chan error)
			go func() {
				resultChannel <- RunTest(test, algo)
			}()

			select {
			case err := <-resultChannel:
				if err != nil {
					failed = true
				}

				if err != nil && !shouldFail {
					t.Errorf("Test failed: %v", err)
				}

			case <-time.After(TestTimeout):
				failed = true
				if !shouldFail {
					t.Errorf("Test timed out")
				}
			}
		})
	}

	if shouldFail && !failed {
		t.Errorf("None of the tests failed")
	}
}
