package tests

import (
	"gitlab.com/slon/shad-ds/causal_broadcast/lib"
)

var ExampleTests = []lib.TestDescriptor{
	{
		Author:    "psushin@yandex-team.ru",
		TestName:  "slow channel",
		NodeCount: 3,
		SendRequests: []lib.SendRequest{
			{
				HappenedBeforeMessages: []int{},
				MessageId:              0,
				Source:                 0,
			},
			{
				HappenedBeforeMessages: []int{0},
				MessageId:              1,
				Source:                 1,
			},
		},
		FailureDescriptors: []lib.FailureDescriptor{},
		DelayFunc: func(message lib.SentMessage) int {
			if message.Source == 0 && message.Destination == 2 {
				return message.DeliverTimestamp + 100
			}
			return message.DeliverTimestamp
		},
	},
	{
		Author:    "psushin@yandex-team.ru",
		TestName:  "dead peer with blocked directions",
		NodeCount: 3,
		SendRequests: []lib.SendRequest{
			{
				HappenedBeforeMessages: []int{},
				MessageId:              0,
				Source:                 0,
			},
			{
				HappenedBeforeMessages: []int{0},
				MessageId:              1,
				Source:                 2,
			},
		},
		FailureDescriptors: []lib.FailureDescriptor{
			{
				HappenedBeforeMessages: []int{0},
				Source:                 2,
				BlockedDirections:      []int{1},
			},
		},
		DelayFunc: func(message lib.SentMessage) int {
			return message.DeliverTimestamp
		},
	},
	{
		Author:    "psushin@yandex-team.ru",
		TestName:  "everybody dies",
		NodeCount: 3,
		SendRequests: []lib.SendRequest{
			{
				HappenedBeforeMessages: []int{},
				MessageId:              0,
				Source:                 0,
			},
			{
				HappenedBeforeMessages: []int{},
				MessageId:              1,
				Source:                 2,
			},
		},
		FailureDescriptors: []lib.FailureDescriptor{
			{
				HappenedBeforeMessages: []int{0},
				Source:                 2,
				BlockedDirections:      []int{1},
			},
			{
				HappenedBeforeMessages: []int{},
				Source:                 1,
				BlockedDirections:      []int{0},
			},
			{
				HappenedBeforeMessages: []int{1},
				Source:                 0,
				BlockedDirections:      []int{2},
			},
		},
		DelayFunc: func(message lib.SentMessage) int {
			return message.DeliverTimestamp
		},
	},
}
