// +build private

package fifo

import (
	"gitlab.com/slon/shad-ds/fifo/algo"
	"gitlab.com/slon/shad-ds/fifo/lib"
	"gitlab.com/slon/shad-ds/fifo/tests"
	algo2 "gitlab.com/slon/shad-ds/private/fifo/algo"
	tests2 "gitlab.com/slon/shad-ds/private/fifo/tests"

	AC130USpectre "gitlab.com/slon/shad-ds/private/fifo/AC130USpectre"
	AlexanderShashkin "gitlab.com/slon/shad-ds/private/fifo/AlexanderShashkin"
	Balantay_ "gitlab.com/slon/shad-ds/private/fifo/Balantay_"
	Gezort "gitlab.com/slon/shad-ds/private/fifo/Gezort"
	Howl "gitlab.com/slon/shad-ds/private/fifo/Howl"
	IbirbyZh "gitlab.com/slon/shad-ds/private/fifo/IbirbyZh"
	IlyaSM "gitlab.com/slon/shad-ds/private/fifo/IlyaSM"
	IvanNechepurenco "gitlab.com/slon/shad-ds/private/fifo/IvanNechepurenco"
	KaEuAn "gitlab.com/slon/shad-ds/private/fifo/KaEuAn"
	LordProtoss "gitlab.com/slon/shad-ds/private/fifo/LordProtoss"
	NandreyN "gitlab.com/slon/shad-ds/private/fifo/NandreyN"
	NotImplemented "gitlab.com/slon/shad-ds/private/fifo/NotImplemented"
	Sul "gitlab.com/slon/shad-ds/private/fifo/Sul"
	Telenkov "gitlab.com/slon/shad-ds/private/fifo/Telenkov"
	ValeriyaSinevich "gitlab.com/slon/shad-ds/private/fifo/ValeriyaSinevich"
	acepeak3 "gitlab.com/slon/shad-ds/private/fifo/acepeak3"
	acherepkov "gitlab.com/slon/shad-ds/private/fifo/acherepkov"
	alesap "gitlab.com/slon/shad-ds/private/fifo/alesap"
	almaz "gitlab.com/slon/shad-ds/private/fifo/almaz"
	andrewsg "gitlab.com/slon/shad-ds/private/fifo/andrewsg"
	artemkaa "gitlab.com/slon/shad-ds/private/fifo/artemkaa"
	artli "gitlab.com/slon/shad-ds/private/fifo/artli"
	asntr "gitlab.com/slon/shad-ds/private/fifo/asntr"
	avinyukhin "gitlab.com/slon/shad-ds/private/fifo/avinyukhin"
	azik "gitlab.com/slon/shad-ds/private/fifo/azik"
	binom "gitlab.com/slon/shad-ds/private/fifo/binom"
	bitcom "gitlab.com/slon/shad-ds/private/fifo/bitcom"
	bixind "gitlab.com/slon/shad-ds/private/fifo/bixind"
	carakan "gitlab.com/slon/shad-ds/private/fifo/carakan"
	chiselko6 "gitlab.com/slon/shad-ds/private/fifo/chiselko6"
	chiyar "gitlab.com/slon/shad-ds/private/fifo/chiyar"
	clockware "gitlab.com/slon/shad-ds/private/fifo/clockware"
	danlark "gitlab.com/slon/shad-ds/private/fifo/danlark"
	dimak24 "gitlab.com/slon/shad-ds/private/fifo/dimak24"
	diralik "gitlab.com/slon/shad-ds/private/fifo/diralik"
	egiby "gitlab.com/slon/shad-ds/private/fifo/egiby"
	eugney_melnikov "gitlab.com/slon/shad-ds/private/fifo/eugney_melnikov"
	fakefeik "gitlab.com/slon/shad-ds/private/fifo/fakefeik"
	filatovartm_cpp "gitlab.com/slon/shad-ds/private/fifo/filatovartm_cpp"
	galtsev "gitlab.com/slon/shad-ds/private/fifo/galtsev"
	gavr97 "gitlab.com/slon/shad-ds/private/fifo/gavr97"
	gigadesu "gitlab.com/slon/shad-ds/private/fifo/gigadesu"
	glebone "gitlab.com/slon/shad-ds/private/fifo/glebone"
	gostkin "gitlab.com/slon/shad-ds/private/fifo/gostkin"
	ig_kholopov "gitlab.com/slon/shad-ds/private/fifo/ig_kholopov"
	igor "gitlab.com/slon/shad-ds/private/fifo/igor"
	ikibardin "gitlab.com/slon/shad-ds/private/fifo/ikibardin"
	jakovenko_dm "gitlab.com/slon/shad-ds/private/fifo/jakovenko_dm"
	jive_jegg "gitlab.com/slon/shad-ds/private/fifo/jive_jegg"
	kborozdin "gitlab.com/slon/shad-ds/private/fifo/kborozdin"
	kkabulov "gitlab.com/slon/shad-ds/private/fifo/kkabulov"
	komissarova_polina_a "gitlab.com/slon/shad-ds/private/fifo/komissarova_polina_a"
	konarkcher "gitlab.com/slon/shad-ds/private/fifo/konarkcher"
	kreo "gitlab.com/slon/shad-ds/private/fifo/kreo"
	leshiy1295 "gitlab.com/slon/shad-ds/private/fifo/leshiy1295"
	makkolts "gitlab.com/slon/shad-ds/private/fifo/makkolts"
	max2103 "gitlab.com/slon/shad-ds/private/fifo/max2103"
	max42 "gitlab.com/slon/shad-ds/private/fifo/max42"
	maximtim "gitlab.com/slon/shad-ds/private/fifo/maximtim"
	mizabrik "gitlab.com/slon/shad-ds/private/fifo/mizabrik"
	mlepekhin "gitlab.com/slon/shad-ds/private/fifo/mlepekhin"
	moskalenkoviktor "gitlab.com/slon/shad-ds/private/fifo/moskalenkoviktor"
	mpivko "gitlab.com/slon/shad-ds/private/fifo/mpivko"
	muradmazitov "gitlab.com/slon/shad-ds/private/fifo/muradmazitov"
	nikita_uvarov "gitlab.com/slon/shad-ds/private/fifo/nikita_uvarov"
	nina "gitlab.com/slon/shad-ds/private/fifo/nina"
	nogo "gitlab.com/slon/shad-ds/private/fifo/nogo"
	nsa92 "gitlab.com/slon/shad-ds/private/fifo/nsa92"
	ogorodnikoff2012 "gitlab.com/slon/shad-ds/private/fifo/ogorodnikoff2012"
	penguin138 "gitlab.com/slon/shad-ds/private/fifo/penguin138"
	ppavel96 "gitlab.com/slon/shad-ds/private/fifo/ppavel96"
	prime "gitlab.com/slon/shad-ds/private/fifo/prime"
	sabyanin "gitlab.com/slon/shad-ds/private/fifo/sabyanin"
	savastep "gitlab.com/slon/shad-ds/private/fifo/savastep"
	sgjurano "gitlab.com/slon/shad-ds/private/fifo/sgjurano"
	shevkunov "gitlab.com/slon/shad-ds/private/fifo/shevkunov"
	sivukhin_nikita "gitlab.com/slon/shad-ds/private/fifo/sivukhin_nikita"
	skrahot "gitlab.com/slon/shad-ds/private/fifo/skrahot"
	ssmike "gitlab.com/slon/shad-ds/private/fifo/ssmike"
	standy "gitlab.com/slon/shad-ds/private/fifo/standy"
	svinpapicha "gitlab.com/slon/shad-ds/private/fifo/svinpapicha"
	thePunchy "gitlab.com/slon/shad-ds/private/fifo/thePunchy"
	tinsane "gitlab.com/slon/shad-ds/private/fifo/tinsane"
	zimmaxim "gitlab.com/slon/shad-ds/private/fifo/zimmaxim"

	"testing"
)

func TestExampleIsNotPassingExtraTests(t *testing.T) {
	lib.RunTests(t, algo.NewExample, tests.ExtraTests, true, false)
}

func TestSolutionIsPassingPrivateTests(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, tests2.Tests, false, false)
}

func TestReferenceIsPassingExtraTests(t *testing.T) {
	lib.RunTests(t, algo2.New, tests.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraIlyaSM(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, IlyaSM.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraartemkaa(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, artemkaa.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraacherepkov(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, acherepkov.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrastandy(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, standy.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrapenguin138(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, penguin138.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrakomissarova_polina_a(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, komissarova_polina_a.ExtraTests, false, false)
}

func TestSolutionIsPassingExtragigadesu(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, gigadesu.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraikibardin(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, ikibardin.ExtraTests, false, false)
}

func TestSolutionIsPassingExtranikita_uvarov(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, nikita_uvarov.ExtraTests, false, false)
}

func TestSolutionIsPassingExtramax2103(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, max2103.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraHowl(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, Howl.ExtraTests, false, false)
}

func TestSolutionIsPassingExtramax42(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, max42.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraalesap(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, alesap.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrashevkunov(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, shevkunov.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraKaEuAn(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, KaEuAn.ExtraTests, false, false)
}

func TestSolutionIsPassingExtramizabrik(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, mizabrik.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrappavel96(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, ppavel96.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraandrewsg(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, andrewsg.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraigor(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, igor.ExtraTests, false, false)
}

func TestSolutionIsPassingExtragaltsev(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, galtsev.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraleshiy1295(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, leshiy1295.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraTelenkov(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, Telenkov.ExtraTests, false, false)
}

func TestSolutionIsPassingExtradimak24(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, dimak24.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraAlexanderShashkin(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, AlexanderShashkin.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraartli(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, artli.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrafilatovartm_cpp(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, filatovartm_cpp.ExtraTests, false, false)
}

func TestSolutionIsPassingExtratinsane(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, tinsane.ExtraTests, false, false)
}

func TestSolutionIsPassingExtragostkin(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, gostkin.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraogorodnikoff2012(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, ogorodnikoff2012.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraNotImplemented(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, NotImplemented.ExtraTests, false, false)
}

func TestSolutionIsPassingExtradanlark(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, danlark.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraazik(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, azik.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrajakovenko_dm(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, jakovenko_dm.ExtraTests, false, false)
}

func TestSolutionIsPassingExtranina(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, nina.ExtraTests, false, false)
}

func TestSolutionIsPassingExtramlepekhin(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, mlepekhin.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrasavastep(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, savastep.ExtraTests, false, false)
}

func TestSolutionIsPassingExtragavr97(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, gavr97.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraclockware(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, clockware.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrasvinpapicha(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, svinpapicha.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrazimmaxim(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, zimmaxim.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrakborozdin(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, kborozdin.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrabinom(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, binom.ExtraTests, false, false)
}

func TestSolutionIsPassingExtramoskalenkoviktor(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, moskalenkoviktor.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrakonarkcher(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, konarkcher.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrabixind(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, bixind.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraBalantay_(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, Balantay_.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrassmike(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, ssmike.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraeugney_melnikov(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, eugney_melnikov.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraIbirbyZh(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, IbirbyZh.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraValeriyaSinevich(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, ValeriyaSinevich.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraIvanNechepurenco(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, IvanNechepurenco.ExtraTests, false, false)
}

func TestSolutionIsPassingExtramaximtim(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, maximtim.ExtraTests, false, false)
}

func TestSolutionIsPassingExtramuradmazitov(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, muradmazitov.ExtraTests, false, false)
}

func TestSolutionIsPassingExtracarakan(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, carakan.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrakkabulov(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, kkabulov.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrathePunchy(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, thePunchy.ExtraTests, false, false)
}

func TestSolutionIsPassingExtranogo(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, nogo.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraig_kholopov(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, ig_kholopov.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrachiselko6(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, chiselko6.ExtraTests, false, false)
}

func TestSolutionIsPassingExtransa92(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, nsa92.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrajive_jegg(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, jive_jegg.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrampivko(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, mpivko.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrasivukhin_nikita(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, sivukhin_nikita.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraLordProtoss(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, LordProtoss.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraasntr(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, asntr.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrachiyar(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, chiyar.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraAC130USpectre(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, AC130USpectre.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraacepeak3(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, acepeak3.ExtraTests, false, false)
}

func TestSolutionIsPassingExtramakkolts(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, makkolts.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrakreo(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, kreo.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraalmaz(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, almaz.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraskrahot(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, skrahot.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraNandreyN(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, NandreyN.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrabitcom(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, bitcom.ExtraTests, false, false)
}

func TestSolutionIsPassingExtradiralik(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, diralik.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrasabyanin(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, sabyanin.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraSul(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, Sul.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraglebone(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, glebone.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrasgjurano(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, sgjurano.ExtraTests, false, false)
}

func TestSolutionIsPassingExtrafakefeik(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, fakefeik.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraGezort(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, Gezort.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraavinyukhin(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, avinyukhin.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraegiby(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, egiby.ExtraTests, false, false)
}

func TestSolutionIsPassingExtraprime(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, prime.ExtraTests, false, false)
}
